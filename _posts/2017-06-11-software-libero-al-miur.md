---
categories:
- community
layout: post
title: Software Libero al MIUR
---
Giovedi 8 giugno una delegazione di rappresentanti del software e della cultura libera sono stati ricevuti a Roma presso il <a href="http://www.istruzione.it/">Ministero dell'Istruzione</a>, per presentare le tante attività che si svolgono in tutta Italia e chiedere un maggior riconoscimento istituzionale degli strumenti, dei contenuti e delle community che, in nome della collaborazione e della cooperazione, sostengono attivamente le scuole.

<a href="https://groups.google.com/forum/#!topic/wii_libera_la_lavagna/Jdf-KXTXyEM">Qui</a> il report di Matteo Ruffoni, coordinatore della community <a href="http://wiildos.it/">Wii Libera la Lavagna</a> e membro del direttivo dell'associazione <a href="http://www.ils.org/">Italian Linux Society</a>.
