---
categories:
- contenuti
layout: post
title: Wikipedia per i Più Piccoli
---
Segnaliamo su <a href="https://it.wikiversity.org/">Wikiversity</a> - sotto-progetto Wikipedia rivolto alla raccolta, alla creazione e alla pubblicazione di materiale ad uso didattico - due pagine speciali, dedicate rispettivamente alla <a href="https://it.wikiversity.org/wiki/Area:Scuola_elementare">Scuola Elementare</a> e alla <a href="https://it.wikiversity.org/wiki/Area:Scuola_media">Scuola Media</a>.

<!--more-->

Benché siano ancora molti i contenuti da redigere, si registrano diverse iniziative (soprattutto in Lombardia, Trentino e Veneto) di insegnanti che hanno scelto di non adottare libri di testo "tradizionali" per il prossimo anno scolastico ma per l'appunto di usare la piattaforma per costruire i propri percorsi didattici. Si prevede pertanto per i prossimi mesi una crescita esponenziale dei contenuti dedicati ai più piccoli accessibili su Wikiversity, da utilizzare, modificare e personalizzare per l'utilizzo in classe (e non solo).
