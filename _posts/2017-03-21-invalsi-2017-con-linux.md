---
categories:
- software
layout: post
title: Invalsi 2017 con Linux
---
Anche quest'anno, grazie all'attenzione della community <a href="http://wiildos.it/">Wii Libera la Lavagna</a>, sarà possibile compilare i dati relativi ai test <a href="http://www.invalsi.it/">Invalsi</a> anche su Linux, per mezzo di una apposita applicazione di cui è possibile chiedere accesso scrivendo all'indirizzo mail <a href="mailto:prove@invalsi.it">prove@invalsi.it</a>.

Purtroppo Invalsi è ostinata nel promuovere, come mezzo preferenziale per queste comunicazioni, un documento XLS imbottito di macro fruibile solo con Microsoft Excel, ignorando qualsivoglia direttiva sulla pluralità informatica e sui formati aperti, e ben si guarda dal pubblicare sul suo sito istituzionale lo strumento alternativo benché esso sia utilizzato con successo già da diversi anni in numerose scuole in tutta Italia. Nonostante questo (o, forse, proprio per questo) le scuole che hanno adottato Linux sono invitate a richiedere l'accesso all'applicazione e a rivolgersi alla mailing list della community Wii Libera la Lavagna per consigli e supporto.
