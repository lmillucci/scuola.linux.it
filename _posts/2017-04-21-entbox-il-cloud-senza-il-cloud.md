---
categories:
- software
layout: post
title: ENTBox, il cloud senza il cloud
---
Segnaliamo oggi <a href="http://entbox.ticedu.fr/">ENTBox</a>, progetto di origine francese che permette di utilizzare, in modo semplice e rapido, un ampio insieme di applicazioni "web" anche in rete locale e dunque anche senza connettività verso Internet.

Predisponendo un RaspberryPI con il software messa a disposizione vengono attivate istanze Wordpress (per la stesura di un blog), Etherpad (per editare collaborativamente testi e appunti), ownCloud (per condividere files), e persino una copia locale di Wikipedia. Lo strumento è ideale, tra le altre cose, per l'utilizzo in aula e l'accesso da parte degli studenti con tablet e smartphones.

Su <a href="http://bibliobox.wikispaces.com/ENTBox">questa pagina</a> si trovano spiegazioni e documentazione in italiano.
