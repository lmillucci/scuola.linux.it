---
categories:
- community
layout: post
title: Linux in Università
---
<a href="http://www.ils.org/">Italian Linux Society</a> ha <a href="http://www.ils.org/unitux">aggiornato</a> il suo <a href="http://www.ils.org/progetti#servizi">"Manuale Operativo per la Community"</a> con alcune indicazioni relative all'avvio di nuovi gruppi di promozione e supporto a Linux all'interno delle facoltà universitarie. L'intento è quello di coinvolgere gli studenti interessati a Linux affinché siano loro in prima persona a divulgare sia gli aspetti tecnici che quelli culturali del freesoftware presso i propri pari.
L'associazione resta a disposizione per suggerimenti e consigli per l'avvio di un nuovo gruppo operativo locale.
