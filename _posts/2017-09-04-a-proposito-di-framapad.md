---
categories:
- software
layout: post
title: A proposito di Framapad...
---
<p>
    Articolo di Frederic Veron (aka @Sangokuss), leggi <a href="https://yoda.nohost.me/svtux/svtuxboxproject/framapad-pedago.html">la pagina originale</a>. Traduzione italiana a cura di Paolo Mauri e di Andrea Primiani.
</p>

<h3>Qualche informazione generale</h3>
<p>
    <a href="https://framapad.org/">Framapad</a> è quello che viene chiamato un "editor di testo collaborativo". Consiste, da un punto di vista tecnico, di un'istanza di <a href="http://etherpad.org/">Etherpad</a> (un software libero) a disposizione su un server a cui sono stati aggiunti diversi <a href="http://static.etherpad.org/plugins.html">plugin</a> che offrono (o no) diverse opzioni (gestione degli account, colori, inserimento di immagini...).<br/>
    Di conseguenza, chiunque abbia a disposizione un server web può installare e attivare i plugin necessari. Per esempio la <a href="https://yoda.nohost.me/svtux/svtuxboxproject/svtuxbox.html">SVTuxBox</a> ha a disposizione il proprio Etherpad.
</p>
<p>
    Il principio di funzionamento è molto semplice: tutti coloro che, con il proprio browser, raggiungono l'indirizzo indicato dall'amministratore (o dall'insegnante) vedranno apparire l'editor di testo. È anche possibile essere presenti in più persone sullo stesso documento. Ciascuno potrà scrivere, non importa in quale luogo, e modificare il lavoro degli altri. Sarà dunque possibile realizzare un lavoro cooperativo e/o collaborativo.<br/>
    Come creare un documento collaborativo? <a href="https://framatube.org/media/tutoriel-framapad">La risposta nel video!</a>
</p>
<p>
    Avrete notato che ho tenuto a distinguere i concetti di cooperazione e collaborazione. Prendiamoci 2 minuti per definirli:
</p>
<ul>
    <li>
        <b>Cooperare</b>, è avere un obiettivo comune ma ciascuno apporta il proprio contributo (senza toccare quello del compagno). Si parla di ripartizione del lavoro.
    </li>
    <li>
        <b>Collaborare</b>: è avere un obiettivo comune ma in questo caso ciascuno modifica (o può modificare) il lavoro del compagno.
    </li>
</ul>
<p>
    La collaborazione ha bisogno quindi di una certa esperienza e quindi di formazione. Per riassumere in poche parole, direi che si comincia sempre attraverso la cooperazione con l'esperienza (e l'apprendimento/accettazione della cancellazione/modifica del proprio lavoro), si arriva a collaborare.
</p>

<h3>Installazione e configurazione di un server</h3>
<p>
    Se, come me, siete utenti Linux, trovere la procedura di installazione è relativamente semplice (anche se...). Vi consiglio di andare a vedere qui il <a href="http://framacloud.org/cultiver-son-jardin/installation-detherpad/">tutorial di installazione Framapad</a>. Per gli altri, non posso che raccomandarvi di utilizzare un server chiavi in mano come quello di Framasoft (Framapad), di Etherpad, dell'Accademia di Versailles.... [<a href="http://pad.wiildos.it/">o quello della community WiiLDOS</a>, NdT]<br/>
    Se non potete avere una connessione internet in classe, vi basterà utilizzare una <a href="https://yoda.nohost.me/svtux/svtuxboxproject/svtuxbox.html">SVTuxBOX</a>! Verrà creata una rete WiFi locale a cui gli studenti potranno connettersi, anche con i loro smartphone per lavorare.
</p>

<h3>Un Framapad... per fare cosa?</h3>
<p>
    Di default, Framapad è un editor di testi semplice, per non dire scarno, che permette di andare all'essenziale. Molto concretamente, questo significa che i vostri alunni non perderanno tempo nella formattazione del testo (sappiamo tutti che la prima volta che uno studente usa un editor di testi come LibreOffice, passa 10 minuti a scegliere il colore dei caratteri, la dimensione....). Di conseguenza il primo utilizzo è la scrittura di un testo.
    <img src="https://yoda.nohost.me/svtux/svtuxboxproject/IMG/framapad_1.png" style="max-width: 100%">
    <img src="https://yoda.nohost.me/svtux/svtuxboxproject/IMG/framapad_classe.png" style="max-width: 100%">
</p>
<p>
    Si possono distinguere diverse modalità di lavoro, per ciascuno di essi la gestione della classe sarà differente:
</p>
<ul>
    <li>
        il gioco <strong>"domande-risposte"</strong>: l'insegnante predispone un testo a cloze (buchi), oppure delle domande preparate in precedenza, gli alunni completano (in classe o a casa).
    </li>
    <li>
        l'insegnante definisce un <strong>"obiettivo comune"</strong> per la classe (o per un gruppo di alunni) e il gruppo si dovrà gestire per raggiungerlo (in classe o a casa), per esempio la stesura di una sintesi del corso TP
    </li>
    <li>
        l'insegnante propone lo strumento in un lavoro di gruppo (<b>ricerca, documentazione</b>....) da realizzare a casa.
    </li>
</ul>
<p>
    Il primo approccio è relativamente poco interessante nel senso che la cooperazione potrà difficilmente attivarsi. Ancora meno la collaborazione. Questo esercizio non ha bisogno (o comunque poco) dell'utilizzo della chat integrata. Al contrario, l'esercizio è molto semplice da gestire da parte dell'insegnante.<br/>
    Il secondo, visto che può prevedere la collaborazione a termine, è molto interessante, ma richiede pratica e pazienza. Al di là delle possibili distrazioni, è indispensabile una particolare attenzione pedagogica da parte dell'insegnante. La pratica dimostra che in questa situazione pedagogica i primi pads sono sempre cooperativi, la collaborazione si attiverà con l'esperienza.<br/>
    Infine, il terzo approccio permette lo sviluppo del gruppo e dell'autonomia. Normalmente gli alunni che hanno provato progressivamente le prime due modalità arrivano a chiedere sempre all'insegnante «come si crea un pad» per lavorare a casa.
</p>
<p>
    Bene, è vero, presentato in questo modo, senza altre spiegazioni, ci si aspetterebbe una &quot;gioiosa confusione&quot;...
    <img src="https://yoda.nohost.me/svtux/svtuxboxproject/IMG/joyeux_bordel.jpg" style="max-width: 100%">
</p>

<h3>La gestione di un gruppo classe (o altro!) su Framapad</h3>
<p>
    La mia esperienza mi porta, in un primo momento, a farvi qualche raccomandazione di buon senso, quasi elementare, riguardo l'organizzazione della classe:
</p>
<ul>
    <li>
        cominciare a far lavorare gli alunni in piccoli gruppi (da 2 a 4 alunni), con un &quot;pad&quot; per gruppo. In questo modo gli alunni possono cooperare e soprattutto riflettere prima di scrivere.
    </li>
    <li>
        poi, vi raccomando di limitare il tempo in cui il pad resterà aperto (per esempio, limitate l'apertura a 55 minuti della lezione ). In questo modo limiterete lo straripamento al di fuori della classe.
    </li>
</ul>
<p>
    Devo essere onesto: queste semplici regole faciliteranno la gestione della classe, ma così limiterete le situazioni a semplice cooperazione nel 99% dei casi!<br/>
    <b>Trucco</b>: una volta creati i pad, controllateli insieme con il programma <a href="https://framaestro.org/">Framaestro</a> per supervisionarli tutti!
    <img src="https://yoda.nohost.me/svtux/svtuxboxproject/IMG/Capture%20d'%C3%A9cran%20de%202017-01-28%2018-24-00.png" style="max-width: 100%">
</p>
<p>
    <b>Aneddoto</b>: qualche anno, quando ho utilizzato Framapad per la prima volta con i miei alunni di 6a [Classe corrispondente alla prima della scuola secondaria di primo grado, NdT] (con una classe intera, 25 partecipanti... che paura, LOL!), che ho avuto l'idea di lasciare il pad aperto 10 giorni (24h/24). Vi confermo che non è da fare la prima volta (a patto di aver bisogno di dormire poco...) perchè vedrete che ci sarà sempre qualcuno sveglio! Come dire: controllare il pad è impossibile, sembra che gli alunni di 6a si alternino giorno e notte per scrivere!
</p>
<p>
    La gestione di un gruppo su Framapad non è mai semplice, ma è possibile trarre alcuni insegnamenti:
</p>
<ul>
    <li>
        Il primo utilizzo in classe dovrà essere sempre senza spiegazione per gli alunni. In effetti dovranno conoscere solo il loro obiettivo comune. Bisogna lasciare che le loro distrazioni si acquietino... (lo so, quando uno è un insegnante non è semplice! LOL)
    </li>
    <li>
        Quando la fase di scoperta è passata (da 5 a 10 minuti?), vi raccomando di prendere il tempo di spiegare che tutto è registrato e a portata di click, niente è veramente cancellato (sia nella chat che nella parte di testo). In breve, una dimostrazione dello storico dinamico e della chat!
    </li>
    <li>
        È importante chiedere una identificazione chiara (iniziali, cognome... a vostra scelta).
    </li>
    <li>
        Essenziale nel processo educativo e nella gestione della classe, bisogna spiegare, se non è stato capito, che la zona del testo è uno spazio di lavoro esclusivo e che la chat permetterà a ciascuno di sincronizzarsi. Vederete che, una volta capito questa differenza, non ci saranno più divagazioni nella zona centrale (Questo può già essere considerato come una riuscita!)
    </li>
    <li>
        L'insegnante dovrà intervenire nella chat (e/o nella zona del testo). Deve fare parte del gruppo.
    </li>
</ul>
<p>
    <b>Mi viene posta spesso una domanda: a cosa serve la chat? Non può essere eliminata?</b> La mia risposta è sempre semplice (e brutale): eliminare la chat significa sopprimere la collaborazione! In effetti la chat è indispensabile per almeno 2 motivi: (1) permette ai partecipanti di mettersi d'accordo, (2) canalizza l'energia dei partecipanti (chiaramente, se le divagazioni sono nella chat, non sono nella zona di lavoro!).
</p>
<p>
    <strong>Aneddoto</strong>: mi ricordo di una volta, con la chat, che mi ha portato a fare molte riflessioni. Lasciatemi raccontare: dopo una sessione con gli alunni della 6a nell'ultima parte della giornata mi accorgo che gli allievi sono molto motivati (come sempre direte voi:-p). Una volta tornato a casa, verso le 18, ancora, gli alunni lavoravano sempre. Lo stesso alle 20. Verso le 22 il loro numero comincia a calare lentamente. Fino a un momento in cui due alunni si sono ritrovati soli. Una ragazza e un ragazzo (che coincidenza...). E naturalmente, i due hanno presto dimenticato che tutte le loro parole erano registrate e visibili. E così, naturlamente, si è creata dell'intesa... Verso le 23, mi sono reso conto e ho bloccato il pad per poi fare un incontro di verifica con la classe il giorno dopo.... Bene, tutto questo per dire: vigilanza! Gli alunni (come gli adulti) dimenticano presto, dietro i loro schermi, che internet non è che un grande luogo pubblico!
</p>
<p>
    <b>Una seconda domanda viene posta regolarmente: quanto si può ottenere una collaborazione?</b> All'inizio è spesso più semplice lavorare con metà della classe (meno di 15 alunni), la mia opinione è piuttosto chiara su questo punto: più il numero dei partecipanti sarà importante, più la collaborazione sarà interessante e pertinente. Chiaramente, quando si è troppo pochi (5-6) si coopera molto più che collaborare. Su questo punto sono quindi in disaccordo con qualche scritto (ma in questi ultimi non è mai chiara la distinzione tra cooperazione e collaborazione.)
</p>
<p>
    Ecco di seguito un esempio di conclusione collaborativa:
    <video src="https://framatube.org/files/1269-framapad-un-exemple-de-travail-collaboratif-avec-d.webm" controls></video>
</p>

<h3>Raccomandazioni</h3>
<p>
    La mia esperienza mi incoraggia molto a farvi queste raccomandazioni:
</p>
<ul>
    <li>
        State calmi, rimanete zen, soprattutto durante le prime sessioni! Si deve saper lasciare andare e accettare le piccole digressioni. A volte sono intervenuto personalmente provocando con un LOL o MDR!
    </li>
    <li>
        Non siate troppo esigente per le prime sessioni: la cooperazione, e più ancora la collaborazione, ha bisogno di pratica.
    </li>
    <li>
        Non lasciate aperto il pad all'inizio. È importante instaurare progressivamente un clima di fiducia.
    </li>
    <li>
        Bisogna assolutamente prendere in considerazione di lavorare con questo programma più volte durante l'anno se si vuole ottenre un padronanza adeguata.
    </li>
    <li>
        Nel momento in cui il pad resterà aperto anche a casa, sarà necessario controllare regolarmente facendo però comprendere agli alunni che non potete essere sempre presenti 24h/24
    </li>
    <li>
        Il programma suscita un entusiasmo e una motivazione forte in tutti gli alunni, sta a voi saper canalizzare questa energia!
    </li>
    <li>
        Potrete osservare rapidamente, sul vostro primo pad, una divisione dei ruoli. È normale e a mio parere da accettare all'inizio. Avrete quindi lo &quot;specialista della redazione&quot;, &quot;il correttore ortografico&quot;&quot;... in seguito potrete cambiare i ruoli senza troppa fatica.
    </li>
</ul>

<h3>Domande frequenti</h3>
<p>
    Ecco alcune domande che mi vengono poste spesso durante le mie presentazioni e/o formazioni:
</p>
<ul>
    <li>
        Perchè non usare GDoc? Framapad offre 3 vantaggi non trascurabili quando si lavora con dei ragazzi: 1) il programma è molto leggero e rapido, la scrittura non ha momenti di latenza; 2) la registrazione è a portata di clic; 3) le opzioni sono molto limitate e semplici, questo permette di concentrarsi prima di tutto sul lavoro da realizzare.
    </li>
    <li>
        Ci sono delle distrazioni? Si e ancora si! Se non ci fossero non sarebbe necessario educare all'utilizzo di questo strumento! Bisogna armarsi di pazienza e di gentilezza, accettando che il modo di comunicare e i codici comunicativi degli alunni sono differenti da quelli degli insegnanti!
    </li>
    <li>
        Può lavorare l'intera classe? Si, ma vi raccomando di cominciare con metà classe o un piccolo gruppo di 4-5 alunni.
    </li>
    <li>
        Tutti gli alunni utilizzano questo strumento? Si! Questo è quello che sorprende all'inizio. Tutta la classe capisce come si deve fare!
    </li>
</ul>
<p>
    <strong>Un'ultima domanda mi viene posta sempre: </strong><strong><i>ma gli alunni lavorano veramente con questa modalità?</i></strong> Si! A volte perfino troppo! Quindi, dal momento che collaborano in 20 sullo stesso pad, si può osservare un comportamento stereotipato: subito si forma un gruppo di alunni e predispongono un catalogo di idee principali, poi modificano quello che hanno preparato.... a tal punto che finiscono per cancellare tutto e ricominciare da capo! E collaborando gli alunni sono molto esigenti. Per verificarlo, chiedete loro di correggere il lavoro del proprio vicino. Vedrete che sono molto più severi di voi!
</p>
<p>
    <strong>Aneddoto: </strong>mi ricordo di una volta (l'anno scorso... XD), quando avevamo ancora metà gruppo in SVT con gli alunni di 6a. Mentre metà classe lavorava in SVT, l'altra faceva tecnologia. Un giorno in cui era assente la mia collega, mentre facevamo un TP poi una sintesi nel pad collaborativo, ho avuto la sorpresa di vedere la mia collega professoressa documentalista (si si, i prof doc sono anche loro degli insegnanti!) connettersi, con il gruppo della mia collega di Tecnologia e lavorare con noi, a distanza! Comodo, no? Bene... improvvisamente le conclusioni del mio TP con il secondo gruppo sono state un po' prevenute. Ma tutti hanno capito che anche a casa loro (per esempio chi era malato) potevano seguire il corso in diretta!
</p>
<p>
    Per concludere (ma, come concludere?), diciamo che vi ho presentato un piccolo assaggio di possibilità: pensate ai vostri alunni DSA, se possono essere facilitati a trovare aiuto con un pad... ecc e a tutto quello che è ancora possibile vivere o immaginare.
</p>

<h3>Per fare di più</h3>
<ul>
    <li>
        <a href="https://framablog.org/2013/06/08/framapad-education-college-svt/">articolo sulla mia esperienza in Framablog</a>
    </li>
    <li>
        <a href="http://eduscol.education.fr/bd/urtic/svt/?commande=aper&amp;id=3420">articolo sul sito accademico di SVT (Eduscol)</a>
    </li>
    <li>
        <a href="https://framadrive.org/index.php/s/UIF8LZzNAVd35ik">analisi pedagogica con la collaborazione di Antoine Taly</a>
    </li>
</ul>
<p>
    Avete a disposizione le informazioni di base per tentare l’avventura. Buona fortuna!
</p>
