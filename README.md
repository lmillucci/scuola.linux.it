# Scuola

Questo è il repository del sito scuola.linux.it, generato con [Jekyll](https://jekyllrb.com/).

Ogni volta che viene effettuata una modifica occorre rieseguire `jekyll build` per ricompilare l'intero sito. In fase di sviluppo è possibile eseguire `bundle exec jekyll serve --livereload`.

Per segnalazioni e richieste, scrivi a webmaster@linux.it
